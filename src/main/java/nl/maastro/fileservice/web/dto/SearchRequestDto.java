package nl.maastro.fileservice.web.dto;

import java.util.HashMap;
import java.util.Map;

public class SearchRequestDto {

    private Map<String, Object> tagChainMust = new HashMap<>();
    private Map<String, Object> tagChainMustNot = new HashMap<>();

    public Map<String, Object> getTagChainMust() {
        return tagChainMust;
    }

    public void setTagChainMust(Map<String, Object> tagChainMust) {
        this.tagChainMust = tagChainMust;
    }

    public Map<String, Object> getTagChainMustNot() {
        return tagChainMustNot;
    }

    public void setTagChainMustNot(Map<String, Object> tagChainMustNot) {
        this.tagChainMustNot = tagChainMustNot;
    }

    @Override
    public String toString() {
        return "SearchRequestDto{" +
                "tagChainMust=" + tagChainMust +
                ", tagChainMustNot=" + tagChainMustNot +
                '}';
    }
}
