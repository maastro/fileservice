package nl.maastro.fileservice.web.controllers;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.fileservice.services.QueryService;

@RestController
@RequestMapping("/api/study")
public class StudyController {
    
    private static final Logger logger = LoggerFactory.getLogger(StudyController.class);

    private final QueryService queryService;

    public StudyController(QueryService queryService) {
        this.queryService = queryService;
    }

    @GetMapping("/{studyInstanceUid}")
    public ResponseEntity<List<String>> getAllImagesInStudy(@PathVariable String studyInstanceUid) throws IOException {
        logger.info("REST request to find all images for studyInstanceUid={}", studyInstanceUid);
        List<String> sopInstanceUids = queryService.findByStudyInstanceUid(studyInstanceUid);
        if (sopInstanceUids == null || sopInstanceUids.isEmpty()) {
            logger.info("No images found for studyInstanceUid={}", studyInstanceUid);
            return ResponseEntity.ok(Collections.emptyList());
        } else {
            return ResponseEntity.ok(sopInstanceUids);
        }
    }
}
