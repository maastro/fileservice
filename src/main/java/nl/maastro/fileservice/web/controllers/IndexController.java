package nl.maastro.fileservice.web.controllers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.services.FileService;

@RestController
@RequestMapping("/api/index")
@EnableConfigurationProperties(FileServiceProperties.class)
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    private final FileService fileService;
    
    private final int defaultStorageTimeInDays;

    public IndexController(FileService fileService, FileServiceProperties fileServiceProperties) {
        this.fileService = fileService;
        this.defaultStorageTimeInDays = fileServiceProperties.getDefaultStorageTimeInDays();
    }

    @GetMapping
    @ApiOperation(value = "Index directory", notes = "Provide a directory path to index, indexing is a background process (check logfile for progress and errors...)")
    public ResponseEntity<Void> indexFilesFromDirectory(@RequestParam Path directory, @RequestParam(required = false) Integer storageTimeInDays) {
        logger.info("REST request to index files from directory=" + directory);
        
        if (!Files.isDirectory(directory)) {
            logger.info("Invalid directory {}", directory);
            return ResponseEntity.badRequest().build();
        }
        
        CompletableFuture.runAsync(() -> {
            try {
                fileService.importFilesFromDirectory(directory, Optional.ofNullable(storageTimeInDays).orElse(defaultStorageTimeInDays));
            } catch (Exception e) {
                throw new CompletionException("Error while indexing files from directory=" + directory, e);
            }
        });

        return ResponseEntity.accepted().build();
    }

}
