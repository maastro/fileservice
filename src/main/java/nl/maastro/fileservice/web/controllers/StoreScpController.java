package nl.maastro.fileservice.web.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;
import nl.maastro.fileservice.services.StoreScpService;

@RestController
@RequestMapping("/api")
@EnableConfigurationProperties(FileServiceProperties.class)
public class StoreScpController {

    private static final Logger logger = LoggerFactory.getLogger(StoreScpController.class);

    private final StoreScpService storeScpService;
    
    private final int defaultStorageTimeInDays;

    public StoreScpController(StoreScpService storeScpService, FileServiceProperties fileServiceProperties) {
        this.storeScpService = storeScpService;
        this.defaultStorageTimeInDays = fileServiceProperties.getDefaultStorageTimeInDays();
    }

    @PutMapping("/storescp/{port}/start")
    @ApiOperation(value = "Start a DICOM C-STORE SCP on a given port")
    public ResponseEntity<StoreScpEntity> startStoreScp(@PathVariable int port, @RequestParam(required = false) Integer storageTimeInDays) {
        logger.info("REST request to start StoreSCP for port=" + port);
        try {
            storageTimeInDays = Optional.ofNullable(storageTimeInDays).orElse(defaultStorageTimeInDays);
            storeScpService.startStoreScp(port, storageTimeInDays);
            return ResponseEntity.ok().build();
        } catch (IOException | GeneralSecurityException e) {
            logger.error("Failed to start StoreSCP for port=" + port, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value = "/storescp/{port}/stop")
    @ApiOperation(value = "Stop a DICOM C-STORE SCP on a given port")
    public ResponseEntity<Void> stopStoreScp(@PathVariable int port) {
        logger.info("REST request to stop StoreSCP for port=" + port);
        StoreScpEntity storeScpEntity = storeScpService.getStoreScpEntity(port);
        if (storeScpEntity == null) {
            logger.info("No StoreSCP found for port=" + port);
            return ResponseEntity.notFound().build();
        } else if (!storeScpEntity.isRunning()) {
            logger.info("StoreSCP was already stopped for port=" + port);
            return ResponseEntity.ok().build();
        }
        storeScpService.stopStoreScp(storeScpEntity);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/storescp/{port}/delete")
    @ApiOperation(value = "Delete a DICOM C-STORE SCP for a given port")
    public ResponseEntity<Void> deleteStoreScp(@PathVariable int port) {
        logger.info("REST request to delete StoreSCP for port=" + port);

        StoreScpEntity storeScpEntity = storeScpService.getStoreScpEntity(port);
        if (storeScpEntity == null) {
            logger.info("No StoreSCP found for port=" + port);
            return ResponseEntity.notFound().build();
        } else if (storeScpEntity.isRunning()) {
            logger.warn("Bad request: cannot delete StoreSCP that is still running");
            return ResponseEntity.badRequest().build();
        }

        List<StoreScpSubscriptionEntity> subscriptions = storeScpEntity.getSubscriptions();
        if (subscriptions != null && !subscriptions.isEmpty()) {
            logger.warn("Bad request: not allowed to delete StoreSCP, because there are outstanding StoreScpSubscriptions"
                    + "; subscriptions=" + subscriptions);
            return ResponseEntity.badRequest().build();
        }

        storeScpService.deleteStoreScpEntity(storeScpEntity);
        return ResponseEntity.ok().build();
    }

}
