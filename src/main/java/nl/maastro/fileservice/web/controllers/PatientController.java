package nl.maastro.fileservice.web.controllers;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.services.DicomWriteService;
import nl.maastro.fileservice.services.QueryService;

@RestController
@RequestMapping("/api/patient")
@EnableConfigurationProperties(FileServiceProperties.class)
public class PatientController {
    
    private static final Logger logger = LoggerFactory.getLogger(PatientController.class);

    private final DicomWriteService dicomWriteService;

    private final QueryService queryService;
    
    private final Path defaultDumpDirectory;

    public PatientController(DicomWriteService dicomWriteService, QueryService queryService, FileServiceProperties fileServiceProperties) {
        this.dicomWriteService = dicomWriteService;
        this.queryService = queryService;
        this.defaultDumpDirectory = fileServiceProperties.getDumpDirectory();
    }

    @GetMapping("/{patientId}")
    public ResponseEntity<List<String>> getAllImagesForPatient(@PathVariable String patientId) throws IOException {
        logger.info("REST request to find images for patientId={}", patientId);
        List<String> sopInstanceUids = queryService.findByPatientId(patientId);
        if (sopInstanceUids == null || sopInstanceUids.isEmpty()) {
            logger.info("No images found for patientId=" + patientId);
            return ResponseEntity.ok(Collections.emptyList());
        } else {
            return ResponseEntity.ok(sopInstanceUids);
        }
    }

    @GetMapping(value = "/{patientId}/write")
    public ResponseEntity<Void> writePatientImages(@PathVariable String patientId,
            @RequestParam(required = false) Path outputDirectory) {
        
        outputDirectory = Optional.ofNullable(outputDirectory).orElse(defaultDumpDirectory);
        logger.info("REST request to write images for patientId=" + patientId + " to outputDirectory=" + outputDirectory);
        try {
            List<String> sopInstanceUids = queryService.findByPatientId(patientId);
            dicomWriteService.writeDicomFiles(sopInstanceUids, outputDirectory);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            logger.error("Failed to write images for patientId=" + patientId, e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
