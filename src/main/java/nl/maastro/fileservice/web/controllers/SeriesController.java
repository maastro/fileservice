package nl.maastro.fileservice.web.controllers;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.fileservice.services.QueryService;

@RestController
@RequestMapping("/api/series")
public class SeriesController {
    
    private static final Logger logger = LoggerFactory.getLogger(SeriesController.class);

	private final QueryService queryService;

	public SeriesController(QueryService queryService) {
		this.queryService = queryService;
	}

	@GetMapping(value = "/{seriesInstanceUid}")
	public ResponseEntity<List<String>> getAllImagesInSeries(@PathVariable String seriesInstanceUid) throws IOException {
		logger.info("REST request to find all images for seriesInstanceUid={}", seriesInstanceUid);
		List<String> sopInstanceUids = queryService.findBySeriesInstanceUid(seriesInstanceUid);
		if (sopInstanceUids == null || sopInstanceUids.isEmpty()) {
            logger.info("No images found for seriesInstanceUid={}", seriesInstanceUid);
            return ResponseEntity.ok(Collections.emptyList());
		} else {
            return ResponseEntity.ok(sopInstanceUids);
		}
	}
}