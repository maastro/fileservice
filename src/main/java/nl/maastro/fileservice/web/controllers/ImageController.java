package nl.maastro.fileservice.web.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.constants.DicomTags;
import nl.maastro.fileservice.services.DicomWriteService;
import nl.maastro.fileservice.services.FileService;
import nl.maastro.fileservice.services.QueryService;
import nl.maastro.fileservice.web.dto.SearchRequestDto;

@RestController
@RequestMapping("/api/image")
@EnableConfigurationProperties(FileServiceProperties.class)
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    private final DicomWriteService dicomWriteService;

    private final QueryService queryService;
    
    private final FileService fileService;
    
    private final ObjectMapper objectMapper;
    
    private final Path defaultDumpDirectory;
    
    public ImageController(DicomWriteService dicomWriteService,
            QueryService queryService,
            FileService fileService,
            ObjectMapper objectMapper,
            FileServiceProperties fileServiceProperties
    ) {
        this.dicomWriteService = dicomWriteService;
        this.queryService = queryService;
        this.fileService = fileService;
        this.objectMapper = objectMapper;
        this.defaultDumpDirectory = fileServiceProperties.getDumpDirectory();
    }

    @DeleteMapping(value = "/{sopInstanceUid}")
    public ResponseEntity<Void> deleteImage(@PathVariable String sopInstanceUid) {
        logger.info("REST request to delete image with sopInstanceUid = " + sopInstanceUid);
        
        try {
            Path filePath = queryService.getFilePath(sopInstanceUid);
            if (filePath == null) {
                logger.warn("Image not found for sopInstanceUid={}", sopInstanceUid);
                return ResponseEntity.notFound().build();
            }
            fileService.deleteFile(sopInstanceUid, filePath);
            return ResponseEntity.ok().build();

        } catch (IOException e) {
            logger.error("Failed to delete image for sopInstanceUid={}", sopInstanceUid, e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping(value = "/{sopInstanceUid}/download")
    public ResponseEntity<Void> downloadImage(@PathVariable String sopInstanceUid, HttpServletResponse response) throws IOException {
        logger.info("GET request to download image with sopInstanceUid = " + sopInstanceUid);

        Path fileLocation = queryService.getFilePath(sopInstanceUid);
        if (fileLocation == null) {
            logger.warn("Image not found for sopInstanceUid={}", sopInstanceUid);
            return ResponseEntity.notFound().build();
        }

        response.setContentType("application/x-download");
        response.setHeader("Content-disposition", "attachment; filename=" + sopInstanceUid + ".dcm");

        InputStream is = new FileInputStream(fileLocation.toFile());
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/search")
    public List<String> searchImages(@RequestBody SearchRequestDto searchRequestDto) throws IOException {
        logger.info("REST request to perform image search: " + searchRequestDto);

        Map<String, Object> tagChainMust = searchRequestDto.getTagChainMust();
        Map<String, Object> tagChainMustNot = searchRequestDto.getTagChainMustNot();

        Map<List<String>, Object> mustMatch = tagChainMust.entrySet().stream()
                .collect(Collectors.toMap(e -> DicomTags.createTagListFromPipes(e.getKey()), Map.Entry::getValue));

        Map<List<String>, Object> mustNotMatch = tagChainMustNot.entrySet().stream()
                .collect(Collectors.toMap(e -> DicomTags.createTagListFromPipes(e.getKey()), Map.Entry::getValue));

        List<String> ids = queryService.findByAttributeValues(mustMatch, mustNotMatch);
        logger.info("Image search returned {} results", ids.size());
        logger.debug("Search result: " + ids);
        return ids;
    }

    @PostMapping(value = "/{sopInstanceUid}/attributes")
    public ResponseEntity<JsonNode> getAttributes(@PathVariable String sopInstanceUid, @RequestBody List<List<String>> dicomTags) {
        logger.info("REST request to get DICOM attributes for sopInstanceUid=" + sopInstanceUid
                + ", dicomTags=" + dicomTags);

        try {
            JsonNode dicomHeader = queryService.getDicomHeader(sopInstanceUid);
            if (dicomHeader == null) {
                logger.warn("DICOM image not found for sopInstanceUid=" + sopInstanceUid);
                return ResponseEntity.notFound().build();
            }
            ObjectNode values = objectMapper.createObjectNode();
            if (dicomTags != null && !dicomTags.isEmpty()) {
                values.setAll(queryService.getChainedDicomTags(dicomHeader, dicomTags));
            }
            return ResponseEntity.ok(values);
            
        } catch (Exception e) {
            logger.error("Failed to get DICOM attributes for sopInstanceUid=" + sopInstanceUid, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/write")
    public ResponseEntity<String> writeAllImages(
            @RequestParam(required = false) Path outputDirectory) {
        
        outputDirectory = Optional.ofNullable(outputDirectory).orElse(defaultDumpDirectory);
        logger.info("REST request to write all images to outputDirectory=" + outputDirectory);
        
        try {
            List<String> sopInstanceUids = queryService.findAll();
            dicomWriteService.writeDicomFiles(sopInstanceUids, outputDirectory);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            logger.error("Failed to write all images", e);
            return ResponseEntity.internalServerError().build();
        }
    }

}
