package nl.maastro.fileservice.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;
import nl.maastro.fileservice.services.StoreScpService;
import nl.maastro.fileservice.services.StoreScpSubscriptionService;

@RestController
@RequestMapping("/api")
public class StoreScpSubscriptionController {
    
    private static final Logger logger = LoggerFactory.getLogger(StoreScpSubscriptionController.class);
    
    private final StoreScpService storeScpService;

    private final StoreScpSubscriptionService storeScpSubscriptionService;
    
    public StoreScpSubscriptionController(StoreScpService storeScpService, StoreScpSubscriptionService storeScpSubscriptionService) {
        this.storeScpService = storeScpService;
        this.storeScpSubscriptionService = storeScpSubscriptionService;
    }
    
    @PostMapping(value = "/storescp/{port}/subscribe")
    @ApiOperation(value = "Subscribe to StoreSCP notifications")
    public ResponseEntity<Void> subscribeToStoreScp(@PathVariable int port, @RequestBody String callbackUrl) {
        logger.info("REST request to subscribe to StoreScp notifications on port " + port + "; callbackUrl=" + callbackUrl);
        StoreScpEntity storeScpEntity = storeScpService.getStoreScpEntity(port);
        if (storeScpEntity == null) {
            logger.info("No StoreSCP found for port=" + port);
            return ResponseEntity.notFound().build();
        } 
        StoreScpSubscriptionEntity subscription = storeScpSubscriptionService.getStoreScpSubscription(storeScpEntity, callbackUrl);
        if (subscription != null) {
            logger.info("Subscription for storeScpEntity " + storeScpEntity + " and callbackUrl " + callbackUrl + " already exists");
            return ResponseEntity.ok().build();
        }
        storeScpSubscriptionService.createStoreScpSubscription(storeScpEntity, callbackUrl);
        return ResponseEntity.ok().build();
    }
    
    @PostMapping(value = "/storescp/{port}/unsubscribe")
    @ApiOperation(value = "Unsubscribe from StoreSCP notifications")
    public ResponseEntity<Void> unsubscribeFromStoreScp(@PathVariable int port, @RequestBody String callbackUrl) {
        logger.info("REST request to unsubscribe from StoreScp notifications on port " + port + "; callbackUrl=" + callbackUrl);
        StoreScpEntity storeScpEntity = storeScpService.getStoreScpEntity(port);
        if (storeScpEntity == null) {
            logger.info("No StoreSCP found for port=" + port);
            return ResponseEntity.notFound().build();
        }
        StoreScpSubscriptionEntity subscription = storeScpSubscriptionService.getStoreScpSubscription(storeScpEntity, callbackUrl);
        if (subscription == null) {
            logger.info("No StoreScpSubscription found for StoreSCP=" + storeScpEntity + " and callbackUrl=" + callbackUrl);
            return ResponseEntity.notFound().build();
        }
        storeScpSubscriptionService.deleteStoreScpSubscription(subscription);
        return ResponseEntity.ok().build();
    }


}
