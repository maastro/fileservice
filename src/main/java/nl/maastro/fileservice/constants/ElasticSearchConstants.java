package nl.maastro.fileservice.constants;

public final class ElasticSearchConstants {

    public static final String INDEX_FILESERVICE = "fileservice";
    public static final String FIELD_DICOMHEADER = "dicomHeader";
    public static final String FIELD_FILE_PATH = "filePath";
    public static final String FIELD_DATE_INDEXED = "dateIndexed";
    public static final String FIELD_EXPIRATION_DATE = "expirationDate";
    public static final String FIELD_VALUE = "Value";
    public static final String FIELD_KEYWORD = "keyword";

}
