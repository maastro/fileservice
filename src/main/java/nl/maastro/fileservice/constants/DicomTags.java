package nl.maastro.fileservice.constants;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class DicomTags {

    public static final List<String> SOP_INSTANCE_UID = Collections.singletonList("00080018");
    public static final List<String> PATIENT_ID = Collections.singletonList("00100020");
    public static final List<String> RTPLAN_LABEL = Collections.singletonList("300A0002");
    public static final List<String> SERIES_INSTANCE_UID = Collections.singletonList("0020000E");
    public static final List<String> STUDY_INSTANCE_UID = Collections.singletonList("0020000D");
    public static final List<String> MODALITY = Collections.singletonList("00080060");

    public static String getFieldNameForDicomTag(List<String> tagChain) {
        String fieldName = ElasticSearchConstants.FIELD_DICOMHEADER;
        for (String tag : tagChain) {
            fieldName += "." + tag + "." + ElasticSearchConstants.FIELD_VALUE;
        }
        return fieldName;
    }

    public static String getKeywordFieldForDicomTag(List<String> tagChain) {
        return getFieldNameForDicomTag(tagChain) + "." + ElasticSearchConstants.FIELD_KEYWORD;
    }

    public static List<String> createTagListFromPipes(String tagChainPipes) {
        return Arrays.asList(tagChainPipes.split("\\|"));
    }

}