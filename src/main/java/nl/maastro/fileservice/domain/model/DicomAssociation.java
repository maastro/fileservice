package nl.maastro.fileservice.domain.model;

import java.util.HashSet;
import java.util.Set;

public class DicomAssociation {
    
    private int port;
    
    private int messageId;
    
    private String calledAeTitle;
    
    private String callingAeTitle;
    
    private Set<SopInstance> sopInstances = new HashSet<>();

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getCalledAeTitle() {
        return calledAeTitle;
    }

    public void setCalledAeTitle(String calledAeTitle) {
        this.calledAeTitle = calledAeTitle;
    }

    public String getCallingAeTitle() {
        return callingAeTitle;
    }

    public void setCallingAeTitle(String callingAeTitle) {
        this.callingAeTitle = callingAeTitle;
    }

    public Set<SopInstance> getSopInstances() {
        return sopInstances;
    }

    public void setSopInstances(Set<SopInstance> sopInstances) {
        this.sopInstances = sopInstances;
    }

    @Override
    public String toString() {
        return "DicomAssociation [port=" + port + ", messageId=" + messageId + ", calledAeTitle=" + calledAeTitle
                + ", callingAeTitle=" + callingAeTitle + "]";
    }
    
}
