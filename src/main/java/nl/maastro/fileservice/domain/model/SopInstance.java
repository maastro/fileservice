package nl.maastro.fileservice.domain.model;

import java.nio.file.Path;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SopInstance {
    
    private String sopClassUid;
    
    private String sopInstanceUid;
    
    @JsonIgnore
    private Path filepath;
    
    public SopInstance(String sopClassUid, String sopInstanceUid, Path filepath) {
        this.sopClassUid = sopClassUid;
        this.sopInstanceUid = sopInstanceUid;
        this.filepath = filepath;
    }

    public String getSopClassUid() {
        return sopClassUid;
    }

    public void setSopClassUid(String sopClassUid) {
        this.sopClassUid = sopClassUid;
    }

    public String getSopInstanceUid() {
        return sopInstanceUid;
    }

    public void setSopInstanceUid(String sopInstanceUid) {
        this.sopInstanceUid = sopInstanceUid;
    }

    public Path getFilepath() {
        return filepath;
    }

    public void setFilepath(Path filepath) {
        this.filepath = filepath;
    }

    @Override
    public String toString() {
        return "SopInstance [sopClassUid=" + sopClassUid + ", sopInstanceUid=" + sopInstanceUid + ", filepath="
                + filepath + "]";
    }

}
