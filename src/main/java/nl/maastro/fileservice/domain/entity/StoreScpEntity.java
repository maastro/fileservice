package nl.maastro.fileservice.domain.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class StoreScpEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private int port;

    private boolean isRunning;

    private int storageTimeInDays;

    @OneToMany(mappedBy = "storeScpEntity")
    private List<StoreScpSubscriptionEntity> subscriptions;

    public int getStorageTimeInDays() {
        return storageTimeInDays;
    }

    public void setStorageTimeInDays(int storageTimeInDays) {
        this.storageTimeInDays = storageTimeInDays;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public List<StoreScpSubscriptionEntity> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<StoreScpSubscriptionEntity> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @Override
    public String toString() {
        return "StoreScpEntity{" +
                "id=" + id +
                ", port=" + port +
                ", isRunning=" + isRunning +
                ", storageTimeInDays=" + storageTimeInDays +
                ", subscriptions=" + subscriptions +
                '}';
    }
}
