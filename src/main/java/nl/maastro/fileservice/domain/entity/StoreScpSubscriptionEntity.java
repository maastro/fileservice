package nl.maastro.fileservice.domain.entity;

import javax.persistence.*;

@Table(
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"store_scp_entity_id", "callback_url"})
)
@Entity
public class StoreScpSubscriptionEntity {

    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    @JoinColumn(name = "store_scp_entity_id")
    private StoreScpEntity storeScpEntity;

    @Column(name = "callback_url")
    private String callbackUrl;

    public StoreScpSubscriptionEntity(StoreScpEntity storeScpEntity, String callbackUrl) {
        this.storeScpEntity = storeScpEntity;
        this.callbackUrl = callbackUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StoreScpSubscriptionEntity() { }

    public StoreScpEntity getStoreScpEntity() {
        return storeScpEntity;
    }

    public void setStoreScpEntity(StoreScpEntity storeScpEntity) {
        this.storeScpEntity = storeScpEntity;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    @Override
    public String toString() {
        return "StoreScpSubscriptionEntity [id=" + id + ", storeScpEntityId=" + storeScpEntity.getId() + ", callbackUrl="
                + callbackUrl + "]";
    }

}
