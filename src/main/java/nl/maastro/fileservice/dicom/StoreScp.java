package nl.maastro.fileservice.dicom;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Dimse;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.fileservice.configuration.properties.StoreScpProperties.DicomTimeoutProperties;
import nl.maastro.fileservice.domain.model.DicomAssociation;
import nl.maastro.fileservice.domain.model.SopInstance;
import nl.maastro.fileservice.services.FileService;
import nl.maastro.fileservice.services.StoreScpSubscriptionService;

public class StoreScp extends nl.maastro.dicomUtils.qr.StoreScp {

    private static final Logger logger = LoggerFactory.getLogger(StoreScp.class);
    
    private final int port;
    
    private final int storageTimeInDays;
    
    private final FileService fileService;

    private final StoreScpSubscriptionService storeScpSubscriptionService;

    private Map<Association, DicomAssociation> openAssociations = new HashMap<>();

    private Map<Association, Path> associationDirectories = new HashMap<>();

    public StoreScp(String aeTitle, int port, Path storageDirectory, int storageTimeInDays, 
            FileService fileService, StoreScpSubscriptionService storeScpSubscriptionService, 
            DicomTimeoutProperties timeoutProperties) throws IOException, GeneralSecurityException {
        super(aeTitle, port, storageDirectory);
        this.port = port;
        this.storageTimeInDays = storageTimeInDays;
        this.fileService = fileService;
        this.storeScpSubscriptionService = storeScpSubscriptionService;
        setIdleTimeout(timeoutProperties.getIdleTimeout());
        setReleaseTimeout(timeoutProperties.getReleaseTimeout());
        setRequestTimeout(timeoutProperties.getRequestTimeout());
    }

    @Override
    public void onDimseRQ(Association as, PresentationContext pc, Dimse dimse,
                          Attributes rq, PDVInputStream data) throws IOException {
        super.onDimseRQ(as, pc, dimse, rq, data);
    }

    @Override
    protected void store(Association as, PresentationContext pc,
                         Attributes rq, PDVInputStream data, Attributes rsp)
            throws IOException {
        if(!this.openAssociations.containsKey(as)) {
            this.openAssociations.put(as, createNewDicomAssociation(as, rq));
        }

        if (!this.associationDirectories.containsKey(as)) {
            this.associationDirectories.put(as, createStorageDirectoryForAssociation());
        }
        super.store(as, pc, rq, data, rsp);
    }

    @Override
    protected File store(Association association, String sopInstanceUid, String sopClassUid,
                         String transferSyntax, PDVInputStream data) throws IOException {
        Path associationDirectory = this.associationDirectories.get(association);
        File file = new File(associationDirectory.toFile(), sopInstanceUid + ".dcm");

        this.store(association, sopInstanceUid, sopClassUid, transferSyntax, data, file);

        DicomAssociation dicomAssociation = this.openAssociations.get(association);
        SopInstance sopInstance = new SopInstance(sopClassUid, sopInstanceUid, file.toPath());
        dicomAssociation.getSopInstances().add(sopInstance);
        return file;
    }

    @Override
    public void onClose(Association association) {
        super.onClose(association);
        DicomAssociation dicomAssociation = openAssociations.remove(association);
        Path associationDirectory = associationDirectories.remove(association);

        process(dicomAssociation);
        associationDirectory.toFile().delete();
    }

    /**
     * This method needs to be synchronized to prevent concurrency errors when having multiple dicom associations
     * at the same time with duplicate dicom images. The synchronization lock ensures that all files of a
     * dicom association are imported before starting processing the next association.
     */
    private synchronized void process(DicomAssociation dicomAssociation) {
        logger.info("Processing DICOM association: " + dicomAssociation);
        for (SopInstance sopInstance : dicomAssociation.getSopInstances()) {
            Path dicomFile = sopInstance.getFilepath();
            try {
                fileService.importFile(dicomFile, storageTimeInDays);
            } catch (Exception e) {
                logger.error("Error while indexing dicomFile {}; file will be skipped", dicomFile, e);
            } finally {
                dicomFile.toFile().delete();
            }
        }
        storeScpSubscriptionService.sendNotifications(dicomAssociation);
    }

    private DicomAssociation createNewDicomAssociation(Association association, Attributes cStoreRequest) {
        DicomAssociation dicomAssociation = new DicomAssociation();
        dicomAssociation.setCalledAeTitle(association.getCalledAET());
        dicomAssociation.setCallingAeTitle(association.getCallingAET());
        dicomAssociation.setMessageId(cStoreRequest.getInt(Tag.MessageID, -1));
        dicomAssociation.setPort(this.port);
        return dicomAssociation;
    }

    private Path createStorageDirectoryForAssociation() throws IOException {
        return Files.createTempDirectory(super.getStorageDirectory(), null);
    }
}
