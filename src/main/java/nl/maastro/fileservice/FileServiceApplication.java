package nl.maastro.fileservice;

import org.elasticsearch.client.ElasticsearchClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@EnableAsync
public class FileServiceApplication {

	private static final Logger logger = LoggerFactory.getLogger(FileServiceApplication.class);
	private static final String NAME = FileServiceApplication.class.getPackage().getName();
	private static final String VERSION = FileServiceApplication.class.getPackage().getImplementationVersion();

	public static void main(String[] args) {
		SpringApplication.run(FileServiceApplication.class, args);
		logger.info("**** FileServiceApplication VERSION **** : " + NAME + " " +  VERSION );
		logger.info("**** ElasticSearch client VERSION **** : " + ElasticsearchClient.class.getPackage().getImplementationVersion());

	}

}