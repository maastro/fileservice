
package nl.maastro.fileservice.configuration;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

	@LoadBalanced
	@Bean
	@Primary
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
}