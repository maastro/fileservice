package nl.maastro.fileservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "file-service")
public class FileServiceProperties {

    private Path tempDirectory = Paths.get("_temp");

    private Path storageDirectory = Paths.get("_data");

    private Path dumpDirectory = Paths.get("dump");

    private int defaultStorageTimeInDays = 30;

    private StoreScpProperties storeScp = new StoreScpProperties();

    private List<String> tagsToIgnore = new ArrayList<>();

    private boolean dataValidationEnabled = true;

    private ScheduledCleanupProperties scheduledCleanup = new ScheduledCleanupProperties();

    public Path getTempDirectory() {
        return tempDirectory;
    }

    public void setTempDirectory(Path tempDirectory) {
        this.tempDirectory = tempDirectory;
    }

    public Path getStorageDirectory() {
        return storageDirectory;
    }

    public void setStorageDirectory(Path storageDirectory) {
        this.storageDirectory = storageDirectory;
    }

    public Path getDumpDirectory() {
        return dumpDirectory;
    }

    public void setDumpDirectory(Path dumpDirectory) {
        this.dumpDirectory = dumpDirectory;
    }

    public int getDefaultStorageTimeInDays() {
        return defaultStorageTimeInDays;
    }

    public void setDefaultStorageTimeInDays(int defaultStorageTimeInDays) {
        this.defaultStorageTimeInDays = defaultStorageTimeInDays;
    }

    public StoreScpProperties getStoreScp() {
        return storeScp;
    }

    public void setStoreScp(StoreScpProperties storeScp) {
        this.storeScp = storeScp;
    }

    public List<String> getTagsToIgnore() {
        return tagsToIgnore;
    }

    public void setTagsToIgnore(List<String> tagsToIgnore) {
        this.tagsToIgnore = tagsToIgnore;
    }

    public ScheduledCleanupProperties getScheduledCleanup() {
        return scheduledCleanup;
    }

    public void setScheduledCleanup(ScheduledCleanupProperties scheduledCleanup) {
        this.scheduledCleanup = scheduledCleanup;
    }

    public boolean isDataValidationEnabled() {
        return dataValidationEnabled;
    }

    public void setDataValidationEnabled(boolean dataValidationEnabled) {
        this.dataValidationEnabled = dataValidationEnabled;
    }
}
