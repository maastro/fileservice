package nl.maastro.fileservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "elasticsearch")
public class ElasticSearchProperties {

    private String host = "localhost";

    private String username = "";

    private String password = "";

    private int port = 9200;

    private int totalFieldsLimit = 8000;

    private int numberOfShards = 1;

    private int numberOfReplicas = 0;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    public int getTotalFieldsLimit() {
        return totalFieldsLimit;
    }

    public void setTotalFieldsLimit(int totalFieldsLimit) {
        this.totalFieldsLimit = totalFieldsLimit;
    }

    public int getNumberOfShards() {
        return numberOfShards;
    }

    public void setNumberOfShards(int numberOfShards) {
        this.numberOfShards = numberOfShards;
    }

    public int getNumberOfReplicas() {
        return numberOfReplicas;
    }

    public void setNumberOfReplicas(int numberOfReplicas) {
        this.numberOfReplicas = numberOfReplicas;
    }

}
