package nl.maastro.fileservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="file-service.store-scp")
public class StoreScpProperties {

    private String aeTitle = "FILESERVICE";

    private DicomTimeoutProperties timeouts = new DicomTimeoutProperties();

    public String getAeTitle() {
        return aeTitle;
    }

    public void setAeTitle(String aeTitle) {
        this.aeTitle = aeTitle;
    }

    public DicomTimeoutProperties getTimeouts() {
        return timeouts;
    }

    public void setTimeouts(DicomTimeoutProperties timeouts) {
        this.timeouts = timeouts;
    }

    public static class DicomTimeoutProperties {

        private static final int NO_TIMEOUT = 0;

        /**
         * Timeout in milliseconds for receiving a DIMSE-RQ after opening an association
         * (no timeout by default)
         */
        private int idleTimeout = NO_TIMEOUT;

        /**
         * Timeout in milliseconds for receiving an association release response (A-RELEASE-RP)
         * after sending an association release request (A-RELEASE_RQ)
         * (no timeout by default)
         */
        private int releaseTimeout = NO_TIMEOUT;

        /**
         * Timeout in milliseconds for receiving an association request (A-ASSOCIATE-RQ)
         * (no timeout by default)
         */
        private int requestTimeout = NO_TIMEOUT;

        public int getIdleTimeout() {
            return idleTimeout;
        }

        public void setIdleTimeout(int idleTimeout) {
            this.idleTimeout = idleTimeout;
        }

        public int getReleaseTimeout() {
            return releaseTimeout;
        }

        public void setReleaseTimeout(int releaseTimeout) {
            this.releaseTimeout = releaseTimeout;
        }

        public int getRequestTimeout() {
            return requestTimeout;
        }

        public void setRequestTimeout(int requestTimeout) {
            this.requestTimeout = requestTimeout;
        }

    }

}
