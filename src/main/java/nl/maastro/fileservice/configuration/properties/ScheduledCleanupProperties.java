package nl.maastro.fileservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="file-service.scheduled-cleanup")
public class ScheduledCleanupProperties {
    
    private boolean enabled = true;
    
    private String cron = "0 0 * * * *";

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }
    
}

