package nl.maastro.fileservice.services;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import nl.maastro.fileservice.configuration.properties.ElasticSearchProperties;
import nl.maastro.fileservice.constants.ElasticSearchConstants;

@Service
@EnableConfigurationProperties(ElasticSearchProperties.class)
public class ElasticSearchService {

    private static final Logger logger = LoggerFactory.getLogger(ElasticSearchService.class);
    private final CredentialsProvider credentialsProvider;
    private final RestHighLevelClient client;
    private final ElasticSearchProperties elasticSearchProperties;


    public ElasticSearchService(ElasticSearchProperties elasticSearchProperties) {
        this.elasticSearchProperties = elasticSearchProperties;
        this.credentialsProvider = new BasicCredentialsProvider();

        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(elasticSearchProperties.getUsername(), elasticSearchProperties.getPassword()));

        RestClientBuilder builder = RestClient.builder(new HttpHost(elasticSearchProperties.getHost(), elasticSearchProperties.getPort(), "http"))
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));

        this.client = new RestHighLevelClient(builder);
    }

    @PostConstruct
    public void init() throws Exception {
        try {
            if (!indexExists()) {
                this.createIndex();
            }

            GetIndexRequest request = new GetIndexRequest(ElasticSearchConstants.INDEX_FILESERVICE);
            GetIndexResponse getIndexResponse = client.indices().get(request, RequestOptions.DEFAULT);
            Settings indexSettings = getIndexResponse.getSettings().get(ElasticSearchConstants.INDEX_FILESERVICE);
            Integer numberOfFields = indexSettings.getAsInt("index.mapping.total_fields.limit", null);

            if (numberOfFields != elasticSearchProperties.getTotalFieldsLimit()) {
                updateTotalFieldsLimit(elasticSearchProperties.getTotalFieldsLimit());
            }
        } catch (Exception e) {
            throw new Exception("Unable to verify ElasticSearch index: " + ElasticSearchConstants.INDEX_FILESERVICE + " --> IS ELASTICSEARCH RUNNING?", e);
        }
    }

    @PreDestroy
    private void disconnect() throws IOException {
        client.close();
    }


    /*
     * Index creation and configuration
     */

    private CreateIndexResponse createIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(ElasticSearchConstants.INDEX_FILESERVICE);
        request.settings(Settings.builder()
                .put("index.mapping.total_fields.limit", elasticSearchProperties.getTotalFieldsLimit())
                .put("number_of_shards", elasticSearchProperties.getNumberOfShards())
                .put("number_of_replicas", elasticSearchProperties.getNumberOfReplicas())
        );
        return client.indices().create(request, RequestOptions.DEFAULT);
    }

    private boolean indexExists() throws IOException {
        GetIndexRequest request = new GetIndexRequest(ElasticSearchConstants.INDEX_FILESERVICE);
        return client.indices().exists(request, RequestOptions.DEFAULT);
    }

    private AcknowledgedResponse updateTotalFieldsLimit(int limit) throws IOException {
        logger.info("Updating index " + ElasticSearchConstants.INDEX_FILESERVICE + " with index.mapping.total_fields.limit:" + limit);
        UpdateSettingsRequest request = new UpdateSettingsRequest(ElasticSearchConstants.INDEX_FILESERVICE);
        request.settings(Settings.builder()
                .put("index.mapping.total_fields.limit", limit));
        return client.indices().putSettings(request, RequestOptions.DEFAULT);
    }


    /*
     * Creating, adding, getting, deleting documents
     */

    public IndexResponse indexDocument(String jsonDocument, String id) throws Exception {
        logger.info("Indexing document for id=" + id);
        IndexRequest request = new IndexRequest(ElasticSearchConstants.INDEX_FILESERVICE)
                .id(id)
                .source(jsonDocument, XContentType.JSON);
        return client.index(request, RequestOptions.DEFAULT);
    }

    public GetResponse getDocument(String id) throws IOException {
        return getDocument(id, FetchSourceContext.FETCH_SOURCE);
    }

    public GetResponse getDocument(String id, String[] fields) throws IOException {
        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, fields, null);
        return getDocument(id, fetchSourceContext);
    }

    private GetResponse getDocument(String id, FetchSourceContext fetchSourceContext) throws IOException {
        logger.info("Getting document for id=" + id);
        GetRequest request = new GetRequest(ElasticSearchConstants.INDEX_FILESERVICE)
                .id(id)
                .fetchSourceContext(fetchSourceContext);
        return client.get(request, RequestOptions.DEFAULT);
    }

    public DeleteResponse deleteDocument(String id) throws IOException {
        logger.info("Deleting document for id=" + id);
        DeleteRequest deleteRequest = new DeleteRequest(ElasticSearchConstants.INDEX_FILESERVICE, id);
        return client.delete(deleteRequest, RequestOptions.DEFAULT);
    }

    public boolean documentExists(String id) throws IOException {
        GetResponse response = getDocument(id, FetchSourceContext.DO_NOT_FETCH_SOURCE);
        return response.isExists();
    }


    /*
     * Queries
     */

    public SearchResponse search(QueryBuilder query, Scroll scroll, FetchSourceContext fetchSource) throws IOException {
        logger.trace("Performing search query={} with fetchSource={} that includes={} and excludes={}", query, fetchSource.fetchSource(), fetchSource.includes(), fetchSource.excludes());

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(query)
                .fetchSource(fetchSource);

        SearchRequest searchRequest = new SearchRequest()
                .scroll(scroll)
                .indices(ElasticSearchConstants.INDEX_FILESERVICE)
                .source(searchSourceBuilder);

        return client.search(searchRequest, RequestOptions.DEFAULT);
    }

    public SearchResponse scroll(String scrollId, Scroll scroll) throws IOException {
        logger.trace("Performing scroll query for scrollId={}", scrollId);
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId)
                .scroll(scroll);
        return client.scroll(scrollRequest, RequestOptions.DEFAULT);
    }

    public ClearScrollResponse clearScroll(String scrollId) throws IOException {
        logger.trace("Clearing search context for scrollId={}", scrollId);
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        return client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    }

}
