package nl.maastro.fileservice.services;

import java.io.StringWriter;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.stream.JsonGenerator;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.VR;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.json.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.constants.ElasticSearchConstants;

@Service
@EnableConfigurationProperties(FileServiceProperties.class)
public class IndexService {
    
    private static final Logger logger = LoggerFactory.getLogger(IndexService.class);

    private final ElasticSearchService elasticSearchService;

    private final ObjectMapper objectMapper;

    private final Set<Integer> tagsToIgnore;

    public IndexService(ElasticSearchService elasticSearchService, FileServiceProperties fileServiceProperties, ObjectMapper objectMapper) {
        this.elasticSearchService = elasticSearchService;
        this.objectMapper = objectMapper;
        this.tagsToIgnore = fileServiceProperties.getTagsToIgnore().stream()
                .map(tag -> convertHexToInteger(tag))
                .collect(Collectors.toSet());
    }

    public void index(Path dicomFile, int storageTimeInDays) throws Exception {
        logger.info("Indexing dicomFile={}", dicomFile);
        try (DicomInputStream dicomInputStream = new DicomInputStream(dicomFile.toFile())) {
            Attributes dicomHeader = dicomInputStream.readDataset();
            String sopInstanceUid = dicomHeader.getString(Tag.SOPInstanceUID);
            LocalDateTime dateTimeIndexed = LocalDateTime.now();
            LocalDateTime expirationDate = LocalDateTime.now().plusDays(storageTimeInDays);
            ObjectNode document = createJsonDocument(dicomHeader, dicomFile, dateTimeIndexed, expirationDate);
            elasticSearchService.indexDocument(document.toString(), sopInstanceUid);
        }
    }

    private ObjectNode createJsonDocument(Attributes dicomHeader, Path dicomFile, LocalDateTime dateTimeIndexed, 
            LocalDateTime expirationDate) throws JsonProcessingException {
        ObjectNode document = objectMapper.createObjectNode();
        removeAttributes(dicomHeader, tagsToIgnore);
        removeAttributes(dicomHeader, getInlineBinaryTags(dicomHeader));
        JsonNode dicomHeaderJson = convertDicomHeaderToJson(dicomHeader);
        document.set(ElasticSearchConstants.FIELD_DICOMHEADER, dicomHeaderJson);
        document.put(ElasticSearchConstants.FIELD_FILE_PATH, dicomFile.toAbsolutePath().toString());
        document.put(ElasticSearchConstants.FIELD_DATE_INDEXED, dateTimeIndexed.toString());
        document.put(ElasticSearchConstants.FIELD_EXPIRATION_DATE, expirationDate.toString());
        return document;
    }

    private static void removeAttributes(Attributes dicomHeader, Collection<Integer> tagsToRemove) {
        tagsToRemove.forEach(tag -> dicomHeader.remove(tag));
    }

    private static List<Integer> getInlineBinaryTags(Attributes dicomHeader) {
        List<Integer> result = new ArrayList<>();
        for (int tag : dicomHeader.tags()) {
            VR vr = dicomHeader.getVR(tag);
            if (vr != null && vr.isInlineBinary()) {
                result.add(tag);
            }
        }
        return result;
    }

    private JsonNode convertDicomHeaderToJson(Attributes dicomHeader) throws JsonProcessingException {
        StringWriter writer = new StringWriter();
        try (JsonGenerator generator = Json.createGenerator(writer)) {
            JSONWriter jsonWriter = new JSONWriter(generator);
            jsonWriter.write(dicomHeader);
            generator.flush();
        }
        String jsonString = writer.toString();
        return objectMapper.readTree(jsonString);
    }

    private static int convertHexToInteger(String hexValue) {
        return Integer.parseInt(hexValue, 16);
    }

}
