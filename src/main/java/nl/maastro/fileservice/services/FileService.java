package nl.maastro.fileservice.services;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.configuration.properties.ScheduledCleanupProperties;
import org.dcm4che3.data.Tag;
import org.dcm4che3.io.DicomInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@EnableConfigurationProperties(FileServiceProperties.class)
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    private static final String DCM_EXTENSION = ".dcm";

    private static final long DISK_USAGE_WARNING = 80L;
    
    private final Path storageDirectory;

    private final ScheduledCleanupProperties scheduledCleanupProperties;

    private final ElasticSearchService elasticSearchService;

    private final int defaultStorageTimeInDays;

    private final IndexService indexService;

    private final boolean dataValidationEnabled;

    private final QueryService queryService;

    public FileService(ElasticSearchService elasticSearchService, IndexService indexService, QueryService queryService,
                       FileServiceProperties fileServiceProperties) throws IOException {
        this.storageDirectory = fileServiceProperties.getStorageDirectory();
        Files.createDirectories(this.storageDirectory);
        this.scheduledCleanupProperties = fileServiceProperties.getScheduledCleanup();
        this.defaultStorageTimeInDays = fileServiceProperties.getDefaultStorageTimeInDays();
        this.dataValidationEnabled = fileServiceProperties.isDataValidationEnabled();
        this.queryService = queryService;
        this.indexService = indexService;
        this.elasticSearchService = elasticSearchService;
    }

    @PostConstruct
    private void validateElasticSearchIndex() {
        if (!dataValidationEnabled) {
            logger.info("Data validation disabled, skipping validation of Elasticsearch index");
            return;
        }
        logger.info("Starting validation of Elasticsearch index");
        List<String> sopInstanceUids;

        try {
            sopInstanceUids = queryService.findAll();
        } catch (Exception e) {
            logger.error("Failed to retrieve all documents from the Elasticsearch index", e);
            logger.warn("Validation of Elasticsearch index is skipped; index may contain invalid documents");
            return;
        }

        logger.info("Found {} documents to validate", sopInstanceUids.size());
        for (String sopInstanceUid : sopInstanceUids) {
            try {
                Path filePath = queryService.getFilePath(sopInstanceUid);
                if (!Files.exists(filePath)) {
                    logger.info("File not found for document {} (filePath={}); document will be deleted", sopInstanceUid, filePath);
                    elasticSearchService.deleteDocument(sopInstanceUid);
                }
            } catch (Exception e) {
                logger.error("Unexpected error while validing document {};", sopInstanceUid, e);
                continue;
            }
        }
        logger.info("Validation of Elasticsearch index completed");
    }

    @PostConstruct
    private void validateStorageDirectory() {
        if (!dataValidationEnabled) {
            logger.info("Data validation disabled, skipping validation of storage directory");
            return;
        }
        logger.info("Starting validation of storage directory");
        List<Path> files;

        try {
            files = Files.list(storageDirectory).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Unexpected error while trying to list all files in storage directory {}", storageDirectory, e);
            logger.warn("Validation of storage directory is skipped; directory may contain invalid files");
            return;
        }

        logger.info("Found {} files to validate", files.size());
        for (Path filePath : files) {
            try {
                String sopInstanceUid = queryService.findByFilePath(filePath);
                if (sopInstanceUid == null) {
                    logger.info("No document found for file {}; trying to index the file", filePath);
                    try {
                        indexService.index(filePath, defaultStorageTimeInDays);
                    } catch (Exception e) {
                        logger.error("Failed to index file {}; file will be deleted", filePath, e);
                        Files.delete(filePath);
                    }
                }
            } catch (IOException e) {
                logger.error("Unexpected error while validating file {}", filePath, e);
                continue;
            }
        }
        logger.info("Validation of storage directory completed");
    }

    @Scheduled(cron = "${file-service.scheduled-cleanup.cron:0 0 * * * *}")
    public void clean() throws IOException {
        if (!scheduledCleanupProperties.isEnabled())
            return;

        logger.info("Scheduled task to delete files past their expiration date");
        LocalDateTime now = LocalDateTime.now();
        List<String> expiredImages = queryService.findByExpirationDateBefore(now);
        logger.info("Found {} expired files", expiredImages.size());

        for (String sopInstanceUid : expiredImages) {
            Path filePath = queryService.getFilePath(sopInstanceUid);
            deleteFile(sopInstanceUid, filePath);
        }

        logDiskUsage();
    }

    public Set<Path> importFilesFromDirectory(Path directory, int storageTimeInDays) throws IOException {
        Set<Path> files = Files.walk(directory)
                .filter(file -> !Files.isDirectory(file))
                .collect(Collectors.toSet());
        Set<Path> destinationFiles = new HashSet<>();
        for (Path file : files) {
            try {
                Path destinationFile = importFile(file, storageTimeInDays);
                destinationFiles.add(destinationFile);
            } catch (Exception e) {
                logger.error("Failed to import file={}; file will be skipped", file, e);
            }
        }
        return destinationFiles;
    }

    public Path importFile(Path filePath, int storageTimeInDays) throws Exception {
        logger.info("Importing file " + filePath);
        Path destinationFilePath;
        try (DicomInputStream dicomInputStream = new DicomInputStream(filePath.toFile())) {
            String sopInstanceUid = dicomInputStream.getFileMetaInformation().getString(Tag.MediaStorageSOPInstanceUID);
            String destinationFileName = sopInstanceUid + DCM_EXTENSION;
            destinationFilePath = storageDirectory.resolve(destinationFileName);
            Files.copy(filePath, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
        }

        try {
            indexService.index(destinationFilePath, storageTimeInDays);
        } catch (Exception e) {
            logger.error("Failed to index file {}; file will be deleted", destinationFilePath, e);
            Files.delete(destinationFilePath);
        }

        return destinationFilePath;
    }

    public void exportFile(Path filePath, Path destinationDirectory) throws IOException {
        logger.info("Exporting file={} to destinationDirectory={}", filePath, destinationDirectory);
        String fileName = filePath.getFileName().toString();
        Path destinationFilePath = destinationDirectory.resolve(fileName);
        Files.createDirectories(destinationDirectory);
        Files.copy(filePath, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
    }

    public void deleteFile(String sopInstanceUid, Path filePath) throws IOException {
        logger.info("Deleting file {}", filePath);
        Files.delete(filePath);
        elasticSearchService.deleteDocument(sopInstanceUid);
    }
    
    private void logDiskUsage() {
        File disk = storageDirectory.toAbsolutePath().getRoot().toFile();
        long totalDiskSpace = convertBytesToGB(disk.getTotalSpace());
        long freeDiskSpace = convertBytesToGB(disk.getFreeSpace());
        long usedDiskSpace = totalDiskSpace - freeDiskSpace;
        long diskUsage = usedDiskSpace * 100L / totalDiskSpace;
        
        logger.info("Available disk space: {} GB; total disk space: {} GB; disk usage: {}%", 
                freeDiskSpace, totalDiskSpace, diskUsage);
        
        if (diskUsage > DISK_USAGE_WARNING) {
            logger.warn("Disk usage has exceeded {}%", DISK_USAGE_WARNING);
        }
    }
    
    private static long convertBytesToGB(long bytes) {
        final long GIGABYTE = 1024L * 1024L * 1024L;
        return bytes / GIGABYTE;
    }

}
