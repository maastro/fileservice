package nl.maastro.fileservice.services;

import java.io.IOException;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.configuration.properties.StoreScpProperties;
import nl.maastro.fileservice.dicom.StoreScp;
import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.repository.StoreScpRepository;

@Service
@EnableConfigurationProperties(FileServiceProperties.class)
public class StoreScpService {

    private static final Logger logger = LoggerFactory.getLogger(StoreScpService.class);

    private final Path tempDirectory;

    private final StoreScpProperties storeScpProperties;

    private final StoreScpRepository storeScpRepository;
    
    private final FileService fileService;

    private final StoreScpSubscriptionService storeScpSubscriptionService;

    private Map<Integer, StoreScp> activeStoreScps = new HashMap<>();

    public StoreScpService(FileServiceProperties fileServiceProperties, StoreScpRepository storeScpRepository,
                           FileService fileService, StoreScpSubscriptionService storeScpSubscriptionService) {
        this.tempDirectory = fileServiceProperties.getTempDirectory();
        this.storeScpProperties = fileServiceProperties.getStoreScp();
        this.storeScpRepository = storeScpRepository;
        this.fileService = fileService;
        this.storeScpSubscriptionService = storeScpSubscriptionService;
    }

    @EventListener(ApplicationReadyEvent.class)
    protected void initialize() {
        for (StoreScpEntity storeScpEntity : storeScpRepository.findAll()) {
            if (storeScpEntity.isRunning()) {
                try {
                    startStoreScp(storeScpEntity);
                } catch (Exception e) {
                    logger.error("Failed to start StoreSCP for entity=" + storeScpEntity, e);
                    storeScpEntity.setRunning(false);
                    storeScpRepository.save(storeScpEntity);
                }
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public void startStoreScp(int port, int storageTimeInDays) throws IOException, GeneralSecurityException {
        StoreScpEntity storeScpEntity = storeScpRepository.getOneByPort(port);

        if (storeScpEntity == null) {
            storeScpEntity = createStoreScpEntity(port, storageTimeInDays);
        } else if (storeScpEntity.getStorageTimeInDays() != storageTimeInDays) {
            logger.info("storeScpEntity with port {} already exists, updating storageTimeInDays from {} to {}",
                    port, storeScpEntity.getStorageTimeInDays(), storageTimeInDays);
            storeScpEntity.setStorageTimeInDays(storageTimeInDays);
            storeScpRepository.save(storeScpEntity);
        }

        startStoreScp(storeScpEntity);
    }

    private void startStoreScp(StoreScpEntity storeScpEntity) throws IOException, GeneralSecurityException {
        int port = storeScpEntity.getPort();
        int storageTimeInDays = storeScpEntity.getStorageTimeInDays();
        if (activeStoreScps.containsKey(port)) {
            logger.info("StoreSCP is already running for StoreScpEntity=" + storeScpEntity);
            return;
        }
        StoreScp storeScp = createStoreScp(port, storageTimeInDays);
        storeScp.start();
        storeScpEntity.setRunning(true);
        storeScpRepository.save(storeScpEntity);
        activeStoreScps.put(port, storeScp);
        logger.info("StoreSCP started on port " + port);
    }

    public void stopStoreScp(StoreScpEntity storeScpEntity) {
        int port = storeScpEntity.getPort();
        if (!activeStoreScps.containsKey(port)) {
            logger.info("No active StoreSCP found for StoreScpEntity=" + storeScpEntity);
            return;
        }
        StoreScp storeScp = activeStoreScps.get(port);
        storeScp.stop();
        storeScpEntity.setRunning(false);
        storeScpRepository.save(storeScpEntity);
        activeStoreScps.remove(port);
        logger.info("StoreSCP stopped on port " + port);
    }

    private StoreScpEntity createStoreScpEntity(int port, int storageTimeInDays) {
        StoreScpEntity storeScpEntity = new StoreScpEntity();
        storeScpEntity.setPort(port);
        storeScpEntity.setRunning(false);
        storeScpEntity.setStorageTimeInDays(storageTimeInDays);
        storeScpEntity = storeScpRepository.save(storeScpEntity);
        logger.info("Created new StoreScpEntity: " + storeScpEntity);
        return storeScpEntity;
    }

    public StoreScpEntity getStoreScpEntity(int port) {
        return storeScpRepository.getOneByPort(port);
    }

    public void deleteStoreScpEntity(StoreScpEntity storeScpEntity) {
        storeScpRepository.delete(storeScpEntity);
        logger.info("Deleted StoreScpEntity: " + storeScpEntity);
    }

    private StoreScp createStoreScp(int port, int storageTimeInDays) throws IOException, GeneralSecurityException {
        return new StoreScp(storeScpProperties.getAeTitle(), port, tempDirectory, storageTimeInDays,
                fileService, storeScpSubscriptionService, storeScpProperties.getTimeouts());
    }

}