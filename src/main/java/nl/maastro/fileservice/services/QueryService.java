package nl.maastro.fileservice.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.maastro.fileservice.constants.DicomTags;
import nl.maastro.fileservice.constants.ElasticSearchConstants;
import nl.maastro.fileservice.utils.JacksonUtils;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class QueryService {

    private static final Logger logger = LoggerFactory.getLogger(QueryService.class);

    private final ElasticSearchService elasticSearchService;

    private final ObjectMapper objectMapper;

    public QueryService(ElasticSearchService elasticSearchService, ObjectMapper objectMapper) {
        this.elasticSearchService = elasticSearchService;
        this.objectMapper = objectMapper;
    }

    public Path getFilePath(String sopInstanceUid) throws IOException {
        logger.info("Fetching filePath for sopInstanceUid=" + sopInstanceUid);

        String[] fields = new String[]{ElasticSearchConstants.FIELD_FILE_PATH};
        GetResponse response = elasticSearchService.getDocument(sopInstanceUid, fields);

        if (response.isExists()) {
            String filePath = response.getSourceAsMap().get(ElasticSearchConstants.FIELD_FILE_PATH).toString();
            return Paths.get(filePath);
        }

        logger.warn("Document does not exist for id=" + sopInstanceUid);
        return null;
    }

    public JsonNode getDicomHeader(String sopInstanceUid) throws IOException {
        logger.info("Fetching dicomHeader for sopInstanceUid=" + sopInstanceUid);

        String[] fields = new String[]{ElasticSearchConstants.FIELD_DICOMHEADER};
        GetResponse response = elasticSearchService.getDocument(sopInstanceUid, fields);

        if (response.isExists()) {
            String document = response.getSourceAsString();
            JsonNode json = objectMapper.readTree(document);
            return json.get(ElasticSearchConstants.FIELD_DICOMHEADER);
        }

        logger.warn("Document does not exist for id=" + sopInstanceUid);
        return null;
    }

    public List<String> findAll() throws IOException {
        QueryBuilder query = QueryBuilders.matchAllQuery();
        return findBy(query);
    }

    public String findByFilePath(Path filePath) throws IOException {
        String fieldName = ElasticSearchConstants.FIELD_FILE_PATH + "." + ElasticSearchConstants.FIELD_KEYWORD;
        QueryBuilder query = QueryBuilders.termQuery(fieldName, filePath.toAbsolutePath().toString());
        List<String> sopInstanceUids = findBy(query);
        if (sopInstanceUids.isEmpty()) {
            return null;
        } else if (sopInstanceUids.size() == 1) {
            return sopInstanceUids.iterator().next();
        } else {
            logger.warn("Found multiple documents for filePath=" + filePath + "; returning the first hit");
            return sopInstanceUids.iterator().next();
        }
    }

    public List<String> findByExpirationDateBefore(LocalDateTime dateTime) throws IOException {
        QueryBuilder query = QueryBuilders.rangeQuery(ElasticSearchConstants.FIELD_EXPIRATION_DATE).to(dateTime);
        return findBy(query);
    }

    public List<String> findByPatientId(String patientId) throws IOException {
        return findByAttributeValue(DicomTags.PATIENT_ID, patientId);
    }

    public List<String> findBySeriesInstanceUid(String seriesInstanceUid) throws IOException {
        return findByAttributeValue(DicomTags.SERIES_INSTANCE_UID, seriesInstanceUid);
    }

    public List<String> findByStudyInstanceUid(String studyInstanceUid) throws IOException {
        return findByAttributeValue(DicomTags.STUDY_INSTANCE_UID, studyInstanceUid);
    }

    private List<String> findByAttributeValue(List<String> tagChain, Object value) throws IOException {
        String fieldName = DicomTags.getKeywordFieldForDicomTag(tagChain);
        QueryBuilder query = QueryBuilders.termQuery(fieldName, value);
        return findBy(query);
    }

    public List<String> findByAttributeValues(Map<List<String>, Object> mustMatch,
                                              Map<List<String>, Object> mustNotMatch) throws IOException {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        if (mustMatch != null) {
            for (Map.Entry<List<String>, Object> entry : mustMatch.entrySet()) {
                String fieldName = DicomTags.getKeywordFieldForDicomTag(entry.getKey());
                Object value = entry.getValue();
                query.must(QueryBuilders.termQuery(fieldName, value));
            }
        }
        if (mustNotMatch != null) {
            for (Map.Entry<List<String>, Object> entry : mustNotMatch.entrySet()) {
                String fieldName = DicomTags.getKeywordFieldForDicomTag(entry.getKey());
                Object value = entry.getValue();
                query.mustNot(QueryBuilders.termQuery(fieldName, value));
            }
        }
        return findBy(query);
    }

    private List<String> findBy(QueryBuilder query) throws IOException {
        Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
        SearchResponse searchResponse = elasticSearchService.search(query, scroll, FetchSourceContext.DO_NOT_FETCH_SOURCE);
        List<String> ids = new ArrayList<>();
        SearchHit[] hits;
        while ((hits = searchResponse.getHits().getHits()) != null && hits.length > 0) {
            for (SearchHit hit : hits) {
                ids.add(hit.getId());
            }
            searchResponse = elasticSearchService.scroll(searchResponse.getScrollId(), scroll);
        }
        elasticSearchService.clearScroll(searchResponse.getScrollId());
        return ids;
    }


    /*
     * Helper functions
     */

    public ObjectNode getChainedDicomTags(JsonNode dicomHeader, List<List<String>> tagChains) {
        ObjectNode values = objectMapper.createObjectNode();

        for (List<String> tagChain : tagChains) {
            ObjectNode tagChainValue = getChainedDicomTag(dicomHeader, tagChain);
            JacksonUtils.merge(values, tagChainValue);
        }
        return values;
    }

    private ObjectNode getChainedDicomTag(JsonNode mainNode, List<String> tagChain) {
        // tagChain is changing in recursive calls to this method, so we need to make a copy
        List<String> tagChainCopy = new ArrayList<>(tagChain);
        String tag  = tagChainCopy.remove(0).toUpperCase();
        ObjectNode outputNode = objectMapper.createObjectNode();
        JsonNode subNode = null;

        if (mainNode != null && mainNode.has(tag)) {
            subNode = mainNode.get(tag).get("Value");
        }

        if (tagChainCopy.isEmpty() || subNode == null) {
            outputNode.set(tag, subNode);
        } else if (subNode.isArray()) {
            ArrayNode array = objectMapper.createArrayNode();

            for (Iterator<JsonNode> iterator = subNode.elements(); iterator.hasNext(); ) {
                JsonNode subNodeElement = iterator.next();
                array.add(getChainedDicomTag(subNodeElement, tagChainCopy));
            }
            outputNode.set(tag, array);
        } else {
            outputNode.set(tag, getChainedDicomTag(subNode, tagChainCopy));
        }
        return outputNode;
    }

}
