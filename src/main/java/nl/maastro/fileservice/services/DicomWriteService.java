package nl.maastro.fileservice.services;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import nl.maastro.fileservice.constants.DicomTags;

@Service
public class DicomWriteService {

    private static final Logger logger = LoggerFactory.getLogger(DicomWriteService.class);

	private final QueryService queryService;
	
	private final FileService fileService;

    public DicomWriteService(QueryService queryService, FileService fileService) {
        this.queryService = queryService;
        this.fileService = fileService;
    }
    
    @Async
    public void writeDicomFiles(Collection<String> sopInstanceUids, Path outputDirectory) throws Exception {
        for (String sopInstanceUid : sopInstanceUids) {
            writeDicomFile(sopInstanceUid, outputDirectory);
        }
    }
    
    private void writeDicomFile(String sopInstanceUid, Path outputDirectory) throws IOException {
        Path filePath = queryService.getFilePath(sopInstanceUid);

        if (filePath == null) {
            logger.warn("DICOM file not found for sopInstanceUid=" + sopInstanceUid);
            return;
        }

        JsonNode dicomHeader = queryService.getDicomHeader(sopInstanceUid);
        String patientId = dicomHeader.get(DicomTags.PATIENT_ID.get(0)).get("Value").get(0).asText();
        String seriesInstanceUid = dicomHeader.get(DicomTags.SERIES_INSTANCE_UID.get(0)).get("Value").get(0).asText();
        String modality = dicomHeader.get(DicomTags.MODALITY.get(0)).get("Value").get(0).asText();

        writeDicomFile(filePath, patientId, modality, seriesInstanceUid, outputDirectory);
    }
    
    private void writeDicomFile(Path dicomFile, String patientId, String modality, String seriesInstanceUid, Path outputDirectory) throws IOException {
        Path destinationDirectory = Paths.get(outputDirectory.toString(), patientId, modality, seriesInstanceUid);
        fileService.exportFile(dicomFile, destinationDirectory);
    }
}
