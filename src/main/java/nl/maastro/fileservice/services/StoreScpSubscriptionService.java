package nl.maastro.fileservice.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;
import nl.maastro.fileservice.domain.model.DicomAssociation;
import nl.maastro.fileservice.repository.StoreScpSubscriptionRepository;

@Service
public class StoreScpSubscriptionService {
    
    private static final Logger logger = LoggerFactory.getLogger(StoreScpSubscriptionService.class);

    private final StoreScpSubscriptionRepository storeScpSubscriptionRepository;
    
    private final RestTemplate restTemplate;

    public StoreScpSubscriptionService(StoreScpSubscriptionRepository storeScpSubscriptionRepository, RestTemplate restTemplate) {
        this.storeScpSubscriptionRepository = storeScpSubscriptionRepository;
        this.restTemplate = restTemplate;
    }

    public StoreScpSubscriptionEntity createStoreScpSubscription(StoreScpEntity storeScpEntity, String callbackUrl) {
        StoreScpSubscriptionEntity subscriptionEntity = new StoreScpSubscriptionEntity(storeScpEntity, callbackUrl);
        subscriptionEntity = storeScpSubscriptionRepository.save(subscriptionEntity);
        logger.info("Created new StoreScpSubscriptionEntity: " + subscriptionEntity); 
        return subscriptionEntity;
    }
    
    public void deleteStoreScpSubscription(StoreScpSubscriptionEntity storeScpSubscriptionEntity) {
        storeScpSubscriptionRepository.delete(storeScpSubscriptionEntity);
        logger.info("Deleted StoreScpSubscriptionEntity: " + storeScpSubscriptionEntity);
    }
    
    public StoreScpSubscriptionEntity getStoreScpSubscription(StoreScpEntity storeScpEntity, String callbackUrl) {
        return storeScpSubscriptionRepository.getOneByStoreScpEntityAndCallbackUrl(storeScpEntity, callbackUrl);
    }
    
    public void sendNotifications(DicomAssociation dicomAssociation) {
        int port = dicomAssociation.getPort();
        List<StoreScpSubscriptionEntity> subscriptions = storeScpSubscriptionRepository.findAllByStoreScpEntityPort(port);
        logger.info("Notifying " + subscriptions.size() + " subscribers of incoming DICOM association " + dicomAssociation);
        subscriptions.forEach(subscription -> sendNotification(subscription.getCallbackUrl(), dicomAssociation));
    }
    
    private void sendNotification(String callbackUrl, DicomAssociation dicomAssociation) {
        logger.info("Sending DICOM association notification to: " + callbackUrl);
        try {
            restTemplate.postForObject(callbackUrl, dicomAssociation, Void.class);
        } catch (Exception e) {
            logger.error("Failed to send DICOM association notification", e);
        }
    }

}
