package nl.maastro.fileservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.fileservice.domain.entity.StoreScpEntity;

@Repository
public interface StoreScpRepository extends JpaRepository<StoreScpEntity, Long> {

    StoreScpEntity getOneByPort(int port);
    
}


