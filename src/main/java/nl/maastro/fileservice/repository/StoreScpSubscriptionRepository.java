package nl.maastro.fileservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;

import java.util.List;

@Repository
public interface StoreScpSubscriptionRepository extends JpaRepository<StoreScpSubscriptionEntity, Long> {

	List<StoreScpSubscriptionEntity> findAllByStoreScpEntityPort(int port);
	
	StoreScpSubscriptionEntity getOneByStoreScpEntityAndCallbackUrl(StoreScpEntity storeScpEntity, String callbackUrl);
	
}


