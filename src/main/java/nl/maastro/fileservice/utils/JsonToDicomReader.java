package nl.maastro.fileservice.utils;

import java.io.StringReader;

import javax.json.Json;
import javax.json.stream.JsonParser;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.json.JSONReader;

public class JsonToDicomReader {
    
    public static Attributes read(String json) {
        try (StringReader reader = new StringReader(json); JsonParser parser = Json.createParser(reader)) {
            JSONReader jsonReader = new JSONReader(parser);
            return jsonReader.readDataset(null);
        }
    }
    
}
