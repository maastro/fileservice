package nl.maastro.fileservice.web.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.services.QueryService;

public class PatientControllerTests {

    private static final String PATIENT_ID_KNOWN = "12345";
    private static final String PATIENT_ID_UNKNOWN = "98765";
    private static final List<String> PATIENT_IMAGES = Arrays.asList("123.45.678");
    
    private static final ObjectMapper mapper = new ObjectMapper();
    
    private static MockMvc mockMvc;
    
    @BeforeAll
    public static void setup() throws IOException {
        QueryService queryServiceMock = Mockito.mock(QueryService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new PatientController(null, queryServiceMock, new FileServiceProperties())).build();
        Mockito.doReturn(PATIENT_IMAGES).when(queryServiceMock).findByPatientId(PATIENT_ID_KNOWN);
        Mockito.doReturn(Collections.emptyList()).when(queryServiceMock).findByPatientId(PATIENT_ID_UNKNOWN);
    }

    @Test
    public void getAllImagesForPatient_knownPatient_shouldReturnOkAndNonEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/patient/{patientId}", PATIENT_ID_KNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> patientImages = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        assertEquals(PATIENT_IMAGES, patientImages);
    }

    @Test
    public void getAllImagesForPatient_unknownPatient_shouldReturnOkAndEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/patient/{patientId}", PATIENT_ID_UNKNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> patientImages = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        assertEquals(Collections.emptyList(), patientImages);
    }
    
}
