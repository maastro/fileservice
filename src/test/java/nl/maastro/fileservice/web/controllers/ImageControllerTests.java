package nl.maastro.fileservice.web.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.services.FileService;
import nl.maastro.fileservice.services.QueryService;

public class ImageControllerTests {
    
    private static final String SOP_INSTANCE_UID_VALID = "123.45.6789";
    private static final String SOP_INSTANCE_UID_NOT_FOUND = "987.65.4321";
    
    private static MockMvc mockMvc;

    @BeforeAll
    public static void setup() throws IOException {
        QueryService queryServiceMock = Mockito.mock(QueryService.class);
        FileService fileServiceMock = Mockito.mock(FileService.class);
        ImageController imageController = new ImageController(null, queryServiceMock, fileServiceMock, null, new FileServiceProperties());
        mockMvc = MockMvcBuilders.standaloneSetup(imageController).build();
        Mockito.doReturn(null).when(queryServiceMock).getFilePath(SOP_INSTANCE_UID_NOT_FOUND);
        Mockito.doReturn(Paths.get("xxx")).when(queryServiceMock).getFilePath(SOP_INSTANCE_UID_VALID);
    }

    @Test
    public void deleteImage_imageExists_shouldReturnOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/image/{sopInstanceUid}", SOP_INSTANCE_UID_VALID))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void deleteImage_imageDoesNotExist_shouldReturnNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/image/{sopInstanceUid}", SOP_INSTANCE_UID_NOT_FOUND))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}