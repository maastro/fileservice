package nl.maastro.fileservice.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.fileservice.services.QueryService;

public class SeriesControllerTests {

    private static final String SERIES_INSTANCE_UID_KNOWN = "KNOWN_UID";
    private static final String SERIES_INSTANCE_UID_UNKNOWN = "UNKNOWN_UID";
    private static final List<String> IMAGES_IN_SERIES = Arrays.asList("123.45.6789");
    
    private static final ObjectMapper mapper = new ObjectMapper();

    private static MockMvc mockMvc;
    
    @BeforeAll
    public static void setup() throws IOException {
        QueryService queryServiceMock = Mockito.mock(QueryService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new SeriesController(queryServiceMock)).build();
        Mockito.doReturn(IMAGES_IN_SERIES).when(queryServiceMock).findBySeriesInstanceUid(SERIES_INSTANCE_UID_KNOWN);
        Mockito.doReturn(Collections.emptyList()).when(queryServiceMock).findBySeriesInstanceUid(SERIES_INSTANCE_UID_UNKNOWN);
    }

    @Test
    public void getAllImagesInSeries_knownSeries_shouldReturnOkAndNonEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/series/{SeriesInstanceUid}", SERIES_INSTANCE_UID_KNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> images = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        Assertions.assertEquals(IMAGES_IN_SERIES, images);
    }

    @Test
    public void getAllImagesInSeries_unknownSeries_shouldReturnOkAndEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/series/{SeriesInstanceUid}", SERIES_INSTANCE_UID_UNKNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> images = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        Assertions.assertEquals(Collections.emptyList(), images);
    }
}