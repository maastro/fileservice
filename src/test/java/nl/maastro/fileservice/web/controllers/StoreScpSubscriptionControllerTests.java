package nl.maastro.fileservice.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;
import nl.maastro.fileservice.services.StoreScpService;
import nl.maastro.fileservice.services.StoreScpSubscriptionService;

public class StoreScpSubscriptionControllerTests {

    private static final String CALLBACK_URL_VALID = "http://valid.com";
    private static final String CALLBACK_URL_NOT_FOUND = "http://not-found.com";
    private static final int PORT_OK = 100;
    private static final int PORT_NOT_FOUND = 144;
    
    private static MockMvc mockMvc;

    @BeforeAll
    static void setup() {
        StoreScpSubscriptionService subscriptionServiceMock = Mockito.mock(StoreScpSubscriptionService.class);
        StoreScpService storeScpServiceMock = Mockito.mock(StoreScpService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new StoreScpSubscriptionController(storeScpServiceMock, subscriptionServiceMock)).build();
        
        StoreScpEntity storeScpEntity = new StoreScpEntity();
        Mockito.doReturn(storeScpEntity).when(storeScpServiceMock).getStoreScpEntity(PORT_OK);
        Mockito.doReturn(new StoreScpSubscriptionEntity()).when(subscriptionServiceMock).getStoreScpSubscription(storeScpEntity, CALLBACK_URL_VALID);
    }

    @Test
    public void subscribeToStoreScp_PortDoesNotExist_ShouldReturnNotFound() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/subscribe", PORT_NOT_FOUND).content(CALLBACK_URL_VALID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void subscribeToStoreScp_AlreadySubscribed_ShouldReturnOk() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/subscribe", PORT_OK).content(CALLBACK_URL_VALID))
                .andExpect(status().isOk());
    }

    @Test
    public void subscribeToStoreScp_PortFoundNotSubscribed_ShouldReturnOk() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/subscribe", PORT_OK).content(CALLBACK_URL_VALID))
                .andExpect(status().isOk());
    }

    @Test
    public void unsubscribeFromStoreScp_PortDoesNotExist_ShouldReturnNotFound() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/unsubscribe", PORT_NOT_FOUND).content(CALLBACK_URL_VALID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void unsubscribeFromStoreScp_CallbackUrlDoesNotExist_ShouldReturnNotFound() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/unsubscribe", PORT_OK).content(CALLBACK_URL_NOT_FOUND))
                .andExpect(status().isNotFound());
    }

    @Test
    public void unsubscribeToStoreScp_ValidPortAndCallbackUrl_ShouldReturnOk() throws Exception {
        mockMvc.perform(post("/api/storescp/{port}/unsubscribe", PORT_OK).content(CALLBACK_URL_VALID))
                .andExpect(status().isOk());
    }

}