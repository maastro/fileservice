package nl.maastro.fileservice.web.controllers;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.fileservice.services.QueryService;

public class StudyControllerTests {

    private static final String STUDY_INSTANCE_UID_KNOWN = "KNOWN_UID";
    private static final String STUDY_INSTANCE_UID_UNKNOWN = "UNKNOWN_UID";
    private static final List<String> IMAGES_IN_STUDY = Arrays.asList("123.45.6789");
    
    private static final ObjectMapper mapper = new ObjectMapper();;
    
    private static MockMvc mockMvc;

    @BeforeAll
    public static void setup() throws IOException {
        QueryService queryServiceMock = Mockito.mock(QueryService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new StudyController(queryServiceMock)).build();
        doReturn(IMAGES_IN_STUDY).when(queryServiceMock).findByStudyInstanceUid(STUDY_INSTANCE_UID_KNOWN);
        doReturn(Collections.emptyList()).when(queryServiceMock).findByStudyInstanceUid(STUDY_INSTANCE_UID_UNKNOWN);
    }

    @Test
    public void getAllImagesInStudy_knownStudy_shouldReturnOkAndNonEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/study/{studyInstanceUid}", STUDY_INSTANCE_UID_KNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> images = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        Assertions.assertEquals(IMAGES_IN_STUDY, images);
    }

    @Test
    public void getAllImagesInStudy_unknownStudy_shouldReturnOkAndEmptyList() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/study/{studyInstanceUid}", STUDY_INSTANCE_UID_UNKNOWN))
                .andExpect(status().isOk())
                .andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<String> images = mapper.readValue(responseBody, new TypeReference<List<String>>() {});
        Assertions.assertEquals(Collections.emptyList(), images);
    }
}