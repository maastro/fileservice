package nl.maastro.fileservice.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import nl.maastro.fileservice.configuration.properties.FileServiceProperties;
import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.entity.StoreScpSubscriptionEntity;
import nl.maastro.fileservice.services.StoreScpService;

public class StoreScpControllerTests {

    private static final int PORT_OK = 100;
    private static final int PORT_NOT_FOUND = 144;
    private static final int PORT_INTERNAL_SERVER_ERROR = 404;
    private static MockMvc mockMvc;
    private static StoreScpEntity storeScpEntity;

    @BeforeAll
    static void setup() throws GeneralSecurityException, IOException {
        StoreScpService storeScpServiceMock = Mockito.mock(StoreScpService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new StoreScpController(storeScpServiceMock, new FileServiceProperties())).build();
        
        storeScpEntity = new StoreScpEntity();
        storeScpEntity.setPort(PORT_OK);
        storeScpEntity.setRunning(false);
        Mockito.doReturn(storeScpEntity).when(storeScpServiceMock).getStoreScpEntity(PORT_OK);
        Mockito.doThrow(new GeneralSecurityException()).when(storeScpServiceMock).startStoreScp(
                ArgumentMatchers.eq(PORT_INTERNAL_SERVER_ERROR), ArgumentMatchers.anyInt());
    }

    @Test
    public void startStoreScp_ShouldThrowGeneralSecurityException() throws Exception {
        mockMvc.perform(put("/api/storescp/{port}/start", PORT_INTERNAL_SERVER_ERROR))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void startStoreScp_ShouldReturnOk() throws Exception {
        mockMvc.perform(put("/api/storescp/{port}/start", PORT_OK))
                .andExpect(status().isOk());
    }

    @Test
    public void stopStoreScp_ShouldReturnNotFound() throws Exception {
        mockMvc.perform(put("/api/storescp/{port}/stop", PORT_NOT_FOUND))
                .andExpect(status().isNotFound());
    }

    @Test
    public void stopStoreScp_StoreScpIsRunning_ShouldReturnOk() throws Exception {
        storeScpEntity.setRunning(true);
        mockMvc.perform(put("/api/storescp/{port}/stop", PORT_OK))
                .andExpect(status().isOk());
    }

    @Test
    public void stopStoreScp_StoreScpIsNotRunning_ShouldReturnOk() throws Exception {
        storeScpEntity.setRunning(false);
        mockMvc.perform(put("/api/storescp/{port}/stop", PORT_OK))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteStoreScp_ShouldReturnNotFound() throws Exception {
        mockMvc.perform(delete("/api/storescp/{port}/delete", PORT_NOT_FOUND))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteStoreScp_StoreScpIsRunning_ShouldReturnBadRequest() throws Exception {
        storeScpEntity.setRunning(true);
        mockMvc.perform(delete("/api/storescp/{port}/delete", PORT_OK))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteStoreScp_StoreScpHasSubscriptions_ShouldReturnBadRequest() throws Exception {
        List<StoreScpSubscriptionEntity> subscriptions = Arrays.asList(new StoreScpSubscriptionEntity(storeScpEntity,"callback"));
        storeScpEntity.setRunning(false);
        storeScpEntity.setSubscriptions(subscriptions);
        mockMvc.perform(delete("/api/storescp/{port}/delete", PORT_OK))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteStoreScp_StoreScpNotRunningAndNullSubscriptions_ShouldReturnOk() throws Exception {
        storeScpEntity.setRunning(false);
        storeScpEntity.setSubscriptions(null);
        mockMvc.perform(delete("/api/storescp/{port}/delete", PORT_OK))
                .andExpect(status().isOk());
    }

}