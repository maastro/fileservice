package nl.maastro.fileservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.web.client.RestTemplate;

import nl.maastro.fileservice.domain.entity.StoreScpEntity;
import nl.maastro.fileservice.domain.model.DicomAssociation;
import nl.maastro.fileservice.repository.StoreScpRepository;
import nl.maastro.fileservice.repository.StoreScpSubscriptionRepository;

@DataJpaTest
public class StoreScpSubscriptionServiceTests {
    
    private static final int PORT_1 = 6001;
    private static final int PORT_2 = 6002;
    private static final String CALLBACK_URL_1 = "http://callback-url-1"; 
    private static final String CALLBACK_URL_2 = "http://callback-url-2";
    
    @Autowired
    private StoreScpRepository storeScpRepository;
    
    @Autowired
    private StoreScpSubscriptionRepository storeScpSubscriptionRepository;
    
    @Mock
    private RestTemplate restTemplateMock;
    
    private StoreScpSubscriptionService storeScpSubscriptionService;
    
    @BeforeEach
    public void initialize() {
        MockitoAnnotations.openMocks(this);
        storeScpSubscriptionService = new StoreScpSubscriptionService(storeScpSubscriptionRepository, restTemplateMock);
        
        StoreScpEntity storeScp_1 = new StoreScpEntity();
        storeScp_1.setPort(PORT_1);
        storeScp_1 = storeScpRepository.save(storeScp_1);
        
        StoreScpEntity storeScp_2 = new StoreScpEntity();
        storeScp_2.setPort(PORT_2);
        storeScp_2 = storeScpRepository.save(storeScp_2);
        
        storeScpSubscriptionService.createStoreScpSubscription(storeScp_1, CALLBACK_URL_1);
        storeScpSubscriptionService.createStoreScpSubscription(storeScp_2, CALLBACK_URL_2);
    }
    
    @Test
    public void shouldSendNotification() {
        DicomAssociation dicomAssociation = new DicomAssociation();
        dicomAssociation.setPort(PORT_1);
        storeScpSubscriptionService.sendNotifications(dicomAssociation);
        Mockito.verify(restTemplateMock).postForObject(CALLBACK_URL_1, dicomAssociation, Void.class);
        Mockito.verifyNoMoreInteractions(restTemplateMock);
    }
    
}
