# Fileservice

The Fileservice indexes radiotherapy DICOM files (RTDOSE, RTPLAN, RTSTRUCT, CT, PT and MR) in ElasticSearch. This information can be queried through an API. Furthermore, the indexed DICOM headers can be used to create packages of DICOM files that belong together. These can then be written to a separate folder or sent to a third party. The DICOM input can be provided either by reading through a folder structure with DICOM files or by performing a DICOM C-STORE to one or more configurable ports. The Fileservice is part of the [Medical Image Analysis framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home).

## Prerequisites ##

- Java 8
- [ElasticSearch 5.X.X](https://www.elastic.co/downloads/elasticsearch)
- 2 GB RAM (ElasticSearch)
- 1 GB RAM (FileService)
- Modern browser 
 
## Installation ##

### As part of the MIA package ###
See [MIA installation](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Installation) on how to install all MIA services simultaneously.

### As standalone ###
Use [fileservice-dicomsorter-package](https://bitbucket.org/maastro/fileservice-dicomsorter)

## Usage ##

### As part of the MIA package ###

 See [Configuring a first computation](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Configuring%20a%20first%20computation) to use the Fileservice as an entrypoint to the MIA. The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests.

### As standalone or dicomsorter ###

Use [fileservice-dicomsorter-package](https://bitbucket.org/maastro/fileservice-dicomsorter)